# Databricks notebook source
# DBTITLE 1,Run this cell to set config values
# MAGIC %run ./config/workspace_utils

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Checkout notebooks from Git and Import into your Home/_gitlab_projects folder
# MAGIC 
# MAGIC ### USE CAUTION WITH THIS NOTEBOOK.  
# MAGIC Do not use RUN ALL unless you understand what it is doing.
# MAGIC 
# MAGIC This notebook allows you to check out a set of notebooks from gitlab and import them to a new directory in Databricks, without needing to manually configure the databricks-cli on your local machine.  It will overwrite the existing notebooks in Databricks that you specify as the import location (your local working branch), and you will lose all of the revision history on those notebooks.
# MAGIC 
# MAGIC By default, notebooks will get loaded to your personal /Users folder at the path - **Users/{your_user_name}/_gitlab_projects/{git_project_name}/{working_branch}**
# MAGIC 
# MAGIC The git_project_name will be taken from the HTTPS URL that you provide in widget 1, and the working_branch will be taken from the branch you provide in widget 3
# MAGIC 
# MAGIC #### Run this next cell if you do not see the widgets at the top of the notebook.  Do not "Run All", just run this one cell.  Then update the widget values at the top of the notebook.

# COMMAND ----------

# DBTITLE 1,CREATE WIDGETS
dbutils.widgets.text("repo_https_path", "https://gitlab.com/adam.kuchinski/notebook_gitflow.git", "1. GitLab repository path - Should start with https and end with .git ")
dbutils.widgets.text("create_from_branch", "main", "2. Remote Branch to Pull")
dbutils.widgets.text("working_branch", "my_feature_branch", "3. Your Local Working Branch")
dbutils.widgets.combobox("overwrite", "No", ["No","Yes"], f"4. Overwrite existing notebooks?")

# COMMAND ----------

# MAGIC %md
# MAGIC Prerequisites:
# MAGIC  - Must configure your GitLab secrets using the configure_gitlab notebook
# MAGIC  - Update the three widgets at the top of the notebook.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### PREP

# COMMAND ----------

#retrieve values from widgets
repo_https_path = dbutils.widgets.get("repo_https_path")
create_from = dbutils.widgets.get("create_from_branch")
working_branch = dbutils.widgets.get("working_branch")
overwrite = dbutils.widgets.get("overwrite")

nb_git = NotebookGitFlowConfig(repo_https_path,working_branch)  #defined in workspace_utils

print("User Workspace Path:", nb_git.workspace_path)
print("Personal S3 Git Path:", nb_git.cloud_target_path)
print("Project Name:", nb_git.git_project_name)
print("Branch From Remote:", create_from)
print("Working Branch Name:", nb_git.working_branch)

os.environ['REMOTE_ORIGIN'] = nb_git.construct_remote_origin()
os.environ['REPO_NAME'] = nb_git.git_project_name
os.environ['CREATE_FROM'] = create_from
os.environ['WORKING_BRANCH'] = nb_git.working_branch
os.environ['S3_PATH'] = nb_git.cloud_target_path
os.environ['DRIVER_PATH'] = nb_git.driver_target_path

# COMMAND ----------

# DBTITLE 1,Check if local branch already exists.  If it does, you will need to set the 'overwrite' widget = True to continue
if nb_git.cloud_target_path_git_exists():
  if overwrite.lower()=="yes":
    displayHTML(f'<h3>User confirmed overwrite of existing branch: Can continue with clone. </h3>')
  else:
    dbutils.notebook.exit(f"ERROR: Local branch already exists.  Must set the 'overwrite' widget to 'Yes' to continue. PROJECT: {nb_git.git_project_name}  BRANCH: {nb_git.working_branch}")
else:  
    print('Local branch does not exist: Can continue with clone')

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### CLONE REPO AND CHECKOUT BRANCH

# COMMAND ----------

# DBTITLE 1,Delete local repo if it exists
# MAGIC %sh
# MAGIC rm -r $S3_PATH

# COMMAND ----------

# DBTITLE 1,Perform Clone and Checkout
# MAGIC %sh
# MAGIC mkdir -p $DRIVER_PATH
# MAGIC cd $DRIVER_PATH
# MAGIC git clone --verbose $REMOTE_ORIGIN
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git config core.filemode false
# MAGIC git checkout $CREATE_FROM
# MAGIC git checkout -b $WORKING_BRANCH
# MAGIC git remote rm origin
# MAGIC mkdir -p $S3_PATH/$REPO_NAME
# MAGIC cp -r $DRIVER_PATH/$REPO_NAME/ $S3_PATH
# MAGIC rm -r $DRIVER_PATH

# COMMAND ----------

# DBTITLE 1,Check git status of local repo to confirm successful checkout
# MAGIC %sh
# MAGIC cd $S3_PATH/$REPO_NAME
# MAGIC git status

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### IMPORT NOTEBOOKS
# MAGIC 
# MAGIC #### Exercise Caution when running this cell 
# MAGIC 
# MAGIC This will OVERWRITE the `workspace_path` in Databricks
# MAGIC 
# MAGIC You will lose all revision history on the existing notebooks

# COMMAND ----------

if nb_git.workspace_path_exists():
  if overwrite.lower()=="yes":
    displayHTML(f'<h3>User confirmed overwrite of existing folder.  {nb_git.workspace_path} WILL BE OVERWRITTEN AND ALL REVISION HISTORY WILL BE LOST</h3>')
  else:
      dbutils.notebook.exit(f"ERROR: User folder already exists.  Must set the 'overwrite' widget to 'Yes' to continue. PATH:  /_gitlab_projects/{nb_git.git_project_name}/{nb_git.working_branch}")
else:  
    print('User notebook folder does not exist: Can continue with import.')

# COMMAND ----------

workspace_api.import_workspace_dir(source_path = '/'.join([nb_git.cloud_target_path,nb_git.git_project_name]), target_path = nb_git.workspace_path, overwrite = True, exclude_hidden_files=True)

# COMMAND ----------

dbutils.notebook.exit(f"Clone and Checkout completed.  Your Notebook path for this branch is: /_gitlab_projects/{nb_git.git_project_name}/{nb_git.working_branch}")