# Overview 

## Introduction

Our goal is to replicate the best practices of Notebook CI/CD in a Workspace without Repos, while removing the dependency of installing the databricks-cli and git bash on a local machine.

These notebooks can be utilized to support either a Feature Branch Workflow or a Gitflow Workflow, where users makes their changes on personal working branches and then push their personal branches to the central repository, such as GitLab or GitHub.  They then create a Pull Request (or Merge Request) in order to review, resolve conflicts, and merge their changes into a shared branch (such as `main` or `development`)

Read more about [Git Workflows here](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow).

## Requirements:

- Access to a shared file system, such as DBFS.  
    - By default, we will use DBFS as the local git repo for each user, but this location can be changed in `workspace_utils`.
- Need to store Git access credentials in Databricks Secrets 
    - Can create Personal Access Tokens in GitLab or GitHub so you don’t need to store your password (or if using SSO)
- Need to install `databricks-cli` python package 
    - Recommended at the cluster level, but is possible as a notebook scoped library by uncommenting the top cell in `workspace_utils`


## Getting started

Import all of the .py notebook files into your Databricks workspace.  Recommend an Admin put Read-Only copies in a Shared Workspace folder, and then each user can Clone them to have their own copy inside their /Users folder.

# How to Use
## Option 1: Use Tutorial Notebook for Simpler Flow

You can review the instructions in the `Notebook GitFlow Tutorial` notebook, which covers the entire process and makes it easy to follow the recommended steps.  

If you have any issues with the Tutorial notebook, or if you would like more advanced control over the git commands, then you can continue with the following instructions and run each notebook individually.

## Option 2: Use Each Notebook Separately for More Control

### Configuration Step: One-time setup of Git Credentials

Create a GitLab Personal Access Token and use the `configure_gitlab` notebook to create a secret with your credentials.  You should only need to do this one time (until your personal access token expires.)

More information on creating a Personal Access Token can be [found here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

### Step 1: Clone and Checkout a repo

Use the `Clone and Checkout` notebook to clone a GitLab repository to your DBFS file path, and import the Notebooks to your personal /Users folder at the path /_gitlab_projects/git_project_name/branch_name

Must provide a https path ending in .git, and must provide a working branch name to checkout.  If the working branch does not exist, a new local branch will be created in DBFS for you.  You can specify which remote branch the working branch will be created from.  

Example:   
 - create_from_branch = development 
 - working_branch = new_feature_1

Notes on the `Clone and Checkout` notebook:

- This notebook allows you to check out a set of notebooks from gitlab and import them to a new directory in Databricks, without needing to manually configure the databricks-cli on your local machine.
- Do not use RUN ALL unless you understand what it is doing.
- By default, notebooks will get loaded to your personal /Users folder at the path - /_gitlab_projects/git_project_name/working_branch
- If the branch folder already exists in Databricks, you will need to set overwrite="Yes" to overwrite the existing notebooks at that location.  
    - Note that you will lose all of the revision history on those notebooks.  
- Prerequisites:
    - Must configure your GitLab secrets using the configure_gitlab notebook
    - Update the widgets at the top of the notebook.


### Step 2: Make changes to your notebooks in Databricks

Work within the new branch that was created inside your /Users folder.  We recommend only working on one notebook (or closely related set of notebooks) per commit.

### Step 3: Commit and Push your changes

Use the `Commit and Push` notebook, which will look in your /Users folder at the path /_gitlab_projects/git_project_name/branch_name and export all of the notebooks found at this path.

The project name is extracted from the repo path that you provide in the Notebook Widget.

### Step 4: Merge in GitLab

You should utilize the GitLab UI to create a Merge Request to handle any conflicts and merge to a shared branch.  The GitLab Web IDE can work with files up to 200 KB in size.  

# Setup a CI/CD Pipeline

You can utilize the GitLab CI/CD Pipelines to deploy your updated notebooks to a Staging or Production Databricks workspace location.  And you can also execute Databricks Jobs remotely, to ensure that the new changes haven't broken any unit or integration tests.  

For an example of how to configure a Databricks CI/CD Pipeline in GitLab, please see:  https://gitlab.com/adam.kuchinski/notebook_gitflow_sample_cicd


