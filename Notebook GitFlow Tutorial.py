# Databricks notebook source
NOTEBOOK_PATH = "./"
CLONE_NB_PATH=f"{NOTEBOOK_PATH}/Clone and Checkout"
COMMIT_NB_PATH=f"{NOTEBOOK_PATH}/Commit and Push"
CONFIG_NB_PATH=f"{NOTEBOOK_PATH}/config/configure_gitlab"

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Requirements
# MAGIC 
# MAGIC - Need to install databricks-cli.  Recommended to be installed at the cluster level.
# MAGIC - Check that the paths above are correct.  If you have Cloned or Moved this tutorial notebook, then you may need to update the NOTEBOOK_PATH

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Configuration Step: One-time setup of Git Credentials
# MAGIC 
# MAGIC Run this step to configure your GitLab access credentials as Databricks Secrets.  You should only need to do this one time (until your personal access token expires.)
# MAGIC 
# MAGIC More information on creating a Personal Access Token can be [found here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
# MAGIC 
# MAGIC Be sure that you select the read_repository and write_repository scopes when creating your PAT in GitLab.  If you don't do this, you will get an error when you attempt to push changes to a repo.
# MAGIC 
# MAGIC After running this step, delete the GIT_PAT that you entered in the cell below, and then clear the Notebook Revision History

# COMMAND ----------

GIT_NAME="my.username"
GIT_EMAIL="my_email@email.com"
GIT_PAT="XXXX1234"
OVERWRITE_PAT="No"  #"No" or "Yes"

# COMMAND ----------

dbutils.notebook.run(CONFIG_NB_PATH,0,{"git_username": GIT_NAME, "git_email": GIT_EMAIL, "git_token": GIT_PAT, "overwrite": OVERWRITE_PAT})

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Notebook GitFlow
# MAGIC 
# MAGIC ## Step 1: Clone and Checkout
# MAGIC 
# MAGIC You will specify a personal working branch on which to make your changes.  
# MAGIC 
# MAGIC Notebooks will get loaded to your personal /Users folder at the path - **Users/{your_user_name}/_gitlab_projects/{git_project_name}/{working_branch}**
# MAGIC 
# MAGIC #### Exercise Caution when setting OVERWRITE = "YES".
# MAGIC 
# MAGIC This will OVERWRITE any existing notebooks located at the above path in Databricks, and you will lose all revision history on the existing notebooks.

# COMMAND ----------

HTTPS_REPO = "https://gitlab.com/adam.kuchinski/notebook_gitflow.git"   #in GitLab, use "Clone"->"Clone with HTTPS" to retrieve this path
BRANCH_TO_PULL = "main"  #this is the remote branch that you want to branch off of
MY_CHECKOUT_WORKING_BRANCH = "hotfix_1234"  #this is your personal working branch.  give it a unique and descriptive name
OVERWRITE = "No"  # "No" or "Yes"

if OVERWRITE.lower()=="yes":
  displayHTML(f"<h2><strong style='color:red;'>CAUTION: WHEN OVERWRITE=='YES', ANY EXISTING NOTEBOOKS WILL BE OVERWRITTEN AND ALL REVISION HISTORY WILL BE LOST</strong></h2>")

# COMMAND ----------

dbutils.notebook.run(CLONE_NB_PATH,0,{"repo_https_path": HTTPS_REPO, "create_from_branch": BRANCH_TO_PULL, "working_branch": MY_CHECKOUT_WORKING_BRANCH, "overwrite": OVERWRITE})

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Step 2:  Make your changes.  
# MAGIC - Move new notebooks into the path that was printed in the previous cell's output.  
# MAGIC - Or make edits to the existing notebooks in that folder.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Step 3: Commit and Push

# COMMAND ----------

HTTPS_REPO = "https://gitlab.com/adam.kuchinski/notebook_gitflow.git"
MY_COMMIT_WORKING_BRANCH = "hotfix_1234"  #In most cases, this needs to match MY_CHECKOUT_WORKING_BRANCH from Step 1
COMMIT_MSG = "" #leave blank to utilize the default date/time export message, or enter a custom commit message

# COMMAND ----------

dbutils.notebook.run(COMMIT_NB_PATH,0,{"repo_https_path": HTTPS_REPO,  "branch_name": MY_COMMIT_WORKING_BRANCH, "commit_msg": COMMIT_MSG})

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Step 4:  Go to the GitLab and confirm that your changes were pushed to your personal working branch.  
# MAGIC 
# MAGIC #### You could now go back to Step 2 and make more changes.
# MAGIC 
# MAGIC #### When ready, go to the GitLab UI create a Merge Request to merge your personal branch into a Shared branch (such as Main or Development)
# MAGIC 
# MAGIC You should utilize the GitLab UI to handle any conflicts and merge to a shared branch.  The GitLab Web IDE can work with files up to 200 KB in size.
