# Databricks notebook source
# DBTITLE 1,Run this cell to set config values
# MAGIC %run ./config/workspace_utils

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Export Notebooks from your Home/_gitlab_projects folder and commit to GitLab
# MAGIC 
# MAGIC This notebook will look in your /Users folder at the path **Users/{your_user_name}/_gitlab_projects/{git_project_name}/{working_branch}** and export all of the notebooks found at this path.  The project name is extracted from the repo path that you provide in the Widget above.
# MAGIC 
# MAGIC 
# MAGIC Note - you should be able to "Run All" if you have only made changes to existing notebooks or added new notebooks within your branch.  **If you deleted notebooks and/or modified the file structure of the repo**, you will need to add custom shell commands to remove the old notebook files from the local branch.  OR... you could choose to commit and push everything as-is, and then use the GitLab Web IDE to remove the old notebooks.  It is recommended to use the GitLab Web IDE to delete or move existing notebooks within the repo.
# MAGIC 
# MAGIC Prerequisites:
# MAGIC  - Must have previously run the **Clone and Checkout** notebook to clone the remote repo.
# MAGIC  - Must have made your code changes in Users/{your_user_name}/_gitlab_projects/{git_project_name}/{working_branch}
# MAGIC 
# MAGIC #### Run this next cell if you do not see the widgets at the top of the notebook.  Do not "Run All", just run this one cell.  Then update the widget values at the top of the notebook.

# COMMAND ----------

# DBTITLE 1,CREATE WIDGETS
dbutils.widgets.text("repo_https_path", "https://gitlab.com/adam.kuchinski/notebook_gitflow.git", "1. GitLab repository path - Should start with https and end with .git ")
dbutils.widgets.text("branch_name", "my_feature_branch", "2. Your Local Working Branch")
dbutils.widgets.text("commit_msg", "", "3. Commit message.  Leave blank for default date/time export message")

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### PREP

# COMMAND ----------

repo_https_path = dbutils.widgets.get("repo_https_path")
branch_name = dbutils.widgets.get("branch_name")
commit_msg = dbutils.widgets.get("commit_msg")

nb_git = NotebookGitFlowConfig(repo_https_path,branch_name)  #defined in workspace_utils

print("User Workspace Path:", nb_git.workspace_path)
print("Personal Git Path:", nb_git.cloud_target_path)
print("Project Name:", nb_git.git_project_name)
print("Branch Name:", nb_git.working_branch)

os.environ['GIT_BRANCH'] = nb_git.working_branch
os.environ['S3_PATH'] = nb_git.cloud_target_path
os.environ['REPO_NAME'] = nb_git.git_project_name
os.environ['DRIVER_PATH'] = nb_git.driver_target_path
os.environ['REMOTE_ORIGIN'] = nb_git.construct_remote_origin()
os.environ['GIT_USER'] = nb_git.gitlab_username()
os.environ['GIT_EMAIL'] = nb_git.gitlab_email()

displayHTML(f"<h2>Notebooks will be exported from: <strong style='color:red;'>{nb_git.workspace_path}</strong></h2><h2>And will be commited to the branch <strong style='color:red;'>{nb_git.working_branch}</strong> for the <strong style='color:red;'>{nb_git.git_project_name}</strong> repository</h2><h2>Commit Message is: <strong style='color:red;'>{nb_git.construct_commit_msg(commit_msg)}</strong>")

# COMMAND ----------

# MAGIC %md
# MAGIC Confirm that you first ran the "Clone and Checkout" notebook for this repo and branch.  If you have not, then you must go run that notebook first to initialize the local repo.
# MAGIC 
# MAGIC Also confirm that there is a Notebook Folder found in the User's **_gitlab_projects** for the working_branch that you have specified.  If this cannot be found, then there are no notebooks to export and commit.

# COMMAND ----------

if not nb_git.cloud_target_path_git_exists():
  dbutils.notebook.exit(f"ERROR: Could not find local repo.  Must run 'Clone and Checkout' notebook first.  PROJECT: {nb_git.git_project_name}  BRANCH: {nb_git.working_branch}")
  
if not nb_git.workspace_path_exists():
  dbutils.notebook.exit(f"ERROR: Could not find any notebooks in your user folder for this branch.  PATH CHECKED:  /_gitlab_projects/{nb_git.git_project_name}/{nb_git.working_branch}")

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC The next cell ensures that the branch you want to work on is checked out. 
# MAGIC 
# MAGIC This command will fail if you didn't previously run the "Clone and Checkout" notebook for this repo.
# MAGIC 
# MAGIC But it's ok if you see something like this, that is due to S3 storage not allowing appends:
# MAGIC 
# MAGIC ```
# MAGIC error: unable to append to '.git/logs/HEAD': Operation not supported
# MAGIC ```

# COMMAND ----------

# MAGIC %sh
# MAGIC cd $S3_PATH/$REPO_NAME
# MAGIC git checkout $GIT_BRANCH
# MAGIC git status

# COMMAND ----------

# DBTITLE 1,EXPORT NOTEBOOKS FROM USER WORKSPACE
workspace_api.export_workspace_dir(source_path=nb_git.workspace_path, target_path='/'.join([nb_git.cloud_target_path,nb_git.git_project_name]), overwrite=True)

# COMMAND ----------

# MAGIC %md
# MAGIC Need to copy directory to driver to perform commit, as appends are not supported by FUSE mounts:  https://kb.databricks.com/dbfs/errno95-operation-not-supported.html
# MAGIC 
# MAGIC We will then copy it back to S3 when complete

# COMMAND ----------

# DBTITLE 1,Copy local repo to the driver.
# MAGIC %sh
# MAGIC mkdir -p $DRIVER_PATH
# MAGIC cp -r $S3_PATH/$REPO_NAME/ $DRIVER_PATH
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git status

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### ADD, COMMIT and PUSH
# MAGIC 
# MAGIC NOTE: This is where you should add custom shell commands instead of the next cell, if you do not want to commit the entire directory. Recommend creating a clone of this notebook if you start adding custom commands, to keep the original clean.
# MAGIC 
# MAGIC Here are some example commands ( you need to **cd** at the beginning of each new command)
# MAGIC 
# MAGIC To view the entire file structure:
# MAGIC ```
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC ls -R 
# MAGIC ```
# MAGIC 
# MAGIC To delete an old python notebook in the /notebooks/common/ folder named _df_utils:
# MAGIC ```
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC rm ./notebooks/common/_df_utils.py
# MAGIC ```
# MAGIC 
# MAGIC To add a single file to the commit:
# MAGIC ```
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git add ./notebooks/tests/df_utils_tests.py
# MAGIC ```
# MAGIC 
# MAGIC To check the git status:
# MAGIC ```
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git status
# MAGIC ```

# COMMAND ----------

# DBTITLE 1, Add all changes to the commit
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git add .
# MAGIC git status

# COMMAND ----------

# DBTITLE 1,Tell Git who you are. Do this at the project level, not as global
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git config user.name $GIT_USER
# MAGIC git config user.email $GIT_EMAIL
# MAGIC git config http.emptyAuth true
# MAGIC git config --list

# COMMAND ----------

# DBTITLE 1,Generate commit message
#If user does not provide a custom commit message, then the message will state "Databricks export on <current date/time>"
current_commit_message = nb_git.construct_commit_msg(commit_msg)
os.environ['COMMIT_MSG'] = current_commit_message
print(current_commit_message)

# COMMAND ----------

# DBTITLE 1,COMMIT
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git commit -m "$COMMIT_MSG"

# COMMAND ----------

# DBTITLE 1,PUSH
# MAGIC %sh
# MAGIC cd $DRIVER_PATH/$REPO_NAME
# MAGIC git push -f --set-upstream $REMOTE_ORIGIN $GIT_BRANCH

# COMMAND ----------

# MAGIC %md
# MAGIC ### CLEANUP 
# MAGIC Copy the updated repo back to S3 and delete the copy on the driver
# MAGIC 
# MAGIC Should confirm that the Push worked before running this step

# COMMAND ----------

# MAGIC %sh
# MAGIC 
# MAGIC cp -r $DRIVER_PATH/$REPO_NAME/ $S3_PATH
# MAGIC rm -r $DRIVER_PATH/

# COMMAND ----------

# MAGIC %sh
# MAGIC cd $S3_PATH/$REPO_NAME
# MAGIC git status

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC If desired, you could now switch to main/dev branch and delete the feature branch
# MAGIC 
# MAGIC ```
# MAGIC %sh
# MAGIC cd $S3_PATH/$REPO_NAME
# MAGIC git branch
# MAGIC echo ----------
# MAGIC git checkout main
# MAGIC git branch -d $GIT_BRANCH
# MAGIC git branch
# MAGIC ```
# MAGIC 
# MAGIC 
# MAGIC or delete the local clone entirely
# MAGIC 
# MAGIC ```
# MAGIC %sh
# MAGIC rm -r $S3_PATH/$REPO_NAME
# MAGIC ```
# MAGIC 
# MAGIC These actions will not happen automatically, if you would like to run them, copy the appropriate code snippet into a new cell and run it

# COMMAND ----------

dbutils.notebook.exit("SUCCESS: Notebook completed.  Check your remote repo to ensure the changes were pushed.  If not, please review the outputs of the COMMIT and PUSH notebook cells for any errors.")