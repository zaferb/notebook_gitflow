# Databricks notebook source
#recommended to install databricks-cli as cluster scoped library, but can uncomment this line if you need to install it just for these notebooks
#%pip install databricks-cli

# COMMAND ----------

import os
from datetime import datetime
import pytz
from databricks_cli.sdk.api_client import ApiClient
from databricks_cli.workspace.api import WorkspaceApi

#get the access token from the notebook
token = dbutils.notebook.entry_point.getDbutils().notebook().getContext().apiToken().get()

#get the internal url for the workspace
host = dbutils.notebook.entry_point.getDbutils().notebook().getContext().apiUrl().get()

client = ApiClient(token = token, host = host, verify=False)
workspace_api = WorkspaceApi(client)

db_username = dbutils.notebook.entry_point.getDbutils().notebook().getContext().tags().get("user").get()

# COMMAND ----------

class NotebookGitFlowConfig(object):
    def __init__(self, repo_https_path, working_branch): # noqa
        self.repo_https_path = repo_https_path
        self.working_branch = working_branch
        self.git_project_name = self.get_project_name_from_https_path()
        self.workspace_path= f"/Users/{db_username}/_gitlab_projects/{self.git_project_name}/{self.working_branch}"  #the path to create from the Databricks Workspace
        self.cloud_target_path = f"/dbfs/user/{db_username}/git/{self.git_project_name}/{self.working_branch}"
        self.driver_target_path = f"/databricks/driver/git/{db_username}/{self.git_project_name}/{self.working_branch}"
    
    
    def gitlab_username(self):
      return dbutils.secrets.get(db_username, "g_name")
    
    
    def gitlab_email(self):
      return dbutils.secrets.get(db_username, "g_email")


    def get_project_name_from_https_path(self):
      '''
      Parses out the project name from the https clone path
      '''

      strip_project_name_from_repo_path = self.repo_https_path.split('/')[-1]

      sstring = ".git"
      if strip_project_name_from_repo_path.endswith(sstring):
          git_project_name = strip_project_name_from_repo_path[:-(len(sstring))]
      else:
        raise Exception("Provide a valid https path ending in .git")     

      return git_project_name
    
    
    def construct_remote_origin(self):
      '''
      Constructs the remote origin from the https clone path and the configured gitlab secrets
      '''

      https_index = self.repo_https_path.find("https://")+8

      remote_origin = ''.join([self.repo_https_path[:https_index],
                               dbutils.secrets.get(db_username, "g_name"),
                               ':',
                               dbutils.secrets.get(db_username, "g_pass"),
                               '@',
                               repo_https_path[https_index:]])

      return remote_origin

    
    def construct_commit_msg(self, custom_commit_msg):
      '''
      Build the default commit message, if the user has not supplied a custom message
      '''
      if custom_commit_msg.strip() == "":
        return (f'Databricks export on {datetime.now(tz=pytz.utc).strftime("%Y-%m-%d, %H:%M:%S %Z%z")}')
      else:
        return custom_commit_msg
      
      
    def cloud_target_path_git_exists(self):
      '''
      Check to see if a .git subdirectory exists in the cloud_target_path, which indicates that the working_branch has previously been initialized
      '''
      
      path_to_check = f"{self.cloud_target_path.replace('/dbfs','dbfs:')}/{self.git_project_name}/.git/"
      
      try:
        dbutils.fs.ls(path_to_check)
        
      except Exception as e:
        if 'java.io.FileNotFoundException' in str(e):
          print(f'Local git repo does not exist at {path_to_check}')
          return False
        else:
            raise e     
              
      return True
    
    
    def workspace_path_exists(self):
      '''
      Check to see if a subdirectory exists for this project and working_branch in the user's workspace folder
      '''
    
      try:
        workspace_api.list_objects(workspace_path=self.workspace_path)
      
      except Exception as e:
        if 'Not Found for url' in str(e):
          print(f'Workspace Path does not exist at {self.workspace_path}')
          return False
        else:
            raise e 
            
      return True