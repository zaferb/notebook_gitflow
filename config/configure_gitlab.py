# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ## Configure GitLab User Credentials
# MAGIC 
# MAGIC Run this notebook to configure your GitLab access credentials as Databricks Secrets.  You should only need to do this one time (until your personal access token expires.)
# MAGIC 
# MAGIC More information on creating a Personal Access Token can be [found here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
# MAGIC 
# MAGIC Be sure that you select the read_repository and write_repository scopes when creating your PAT in GitLab.  If you don't do this, you will get an error when you attempt to push changes to a repo.

# COMMAND ----------

# Uncomment the command in this cell if you do not already have the databricks-cli installed on the cluster.  
# Note: %pip does not work on ML Clusters, so you would either need to install the databricks-cli as a cluster library, or use %sh to call /databricks/python/bin/pip install databricks-cli 

#%pip install databricks-cli

# COMMAND ----------

# DBTITLE 1,Create Widgets (do not update this code.  Run this cell and then update the widgets that appear.)
dbutils.widgets.text("git_username", "", "1. Git Provider User Name")
dbutils.widgets.text("git_email", "", "2. Git Provider Email Address")
dbutils.widgets.text("git_token", "", "3. Git Personal Access Token")
dbutils.widgets.combobox("overwrite", "No", ["No","Yes"], f"4. Overwrite existing credentials?")

# COMMAND ----------

#get widget values
g_name = dbutils.widgets.get("git_username")
g_email = dbutils.widgets.get("git_email")
g_pass = dbutils.widgets.get("git_token")
overwrite = dbutils.widgets.get("overwrite")

# COMMAND ----------

#instantiate the client using the notebook context
from databricks_cli.sdk.api_client import ApiClient
from databricks_cli.sdk import SecretService

token = dbutils.notebook.entry_point.getDbutils().notebook().getContext().apiToken().get()
host = dbutils.notebook.entry_point.getDbutils().notebook().getContext().apiUrl().get()

client = ApiClient(token = token, host = host, verify=False)
secrets = SecretService(client)

secrets.list_scopes()

#get and confirm the current user name.  A new scope will be created with this name
db_username = dbutils.notebook.entry_point.getDbutils().notebook().getContext().tags().get("user").get()

print(db_username)

# COMMAND ----------

from requests.exceptions import HTTPError

#will throw an error if the scope already exists. If you are trying to update previously created secrets, continue with the next cells
try:
  secrets.create_scope(scope=db_username)
except HTTPError as err:
  print(f"Scope named {db_username} already exists")
  if overwrite.lower()=="yes":
    displayHTML(f'<h3>User confirmed overwrite of existing credentials: Can continue. </h3>')
  else:
    dbutils.notebook.exit(f"ERROR: Git Credentials already exist for user.  Must set the 'overwrite' widget to 'Yes' to continue. DATABRICKS USER: {db_username}  ")

# COMMAND ----------

#confirm that you are the only principal with permission to access your secret
secrets.list_acls(scope=db_username)

# COMMAND ----------

# DBTITLE 1,Create (or update) your personal GitLab configuration secrets
g_name_clean = g_name.strip('@').strip()
g_email_clean = g_email.strip()
g_pass_clean = g_pass.strip()

if g_name_clean == '':
  print("Please enter a valid Git Provider User Name")
elif "@" not in g_email_clean:
  print("Please enter a valid Git Provider Email Address")
elif not g_pass_clean:
  print("Please enter a valid Git Provider Personal Access Token")
else:
  #create or update the three needed secrets to connect to gitlab using https
  print("Updating secrets")
  secrets.put_secret(scope=db_username, key="g_name",string_value=g_name_clean)
  secrets.put_secret(scope=db_username, key="g_email",string_value=g_email_clean)
  secrets.put_secret(scope=db_username, key="g_pass",string_value=g_pass_clean) 

# COMMAND ----------

# DBTITLE 1,Remove the widget with your Personal Access Token, to ensure the value is cleared out
dbutils.widgets.remove("git_token")

# COMMAND ----------

dbutils.notebook.exit(f"SUCCESS: Notebook completed. Git Credentials created for user.  DATABRICKS USER: {db_username}")